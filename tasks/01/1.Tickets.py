from utils.test_util import test


@test()
def count_lucky(n=None):
    '''
    Test 0 PASSED Time: 27mcs
    Test 1 PASSED Time: 103mcs
    Test 2 PASSED Time: 1093mcs
    Test 3 PASSED Time: 12264mcs
    Test 4 PASSED Time: 128506mcs
    Test 5 PASSED Time: 1478795mcs (1,4 s)
    Test 6 PASSED Time: 15722902mcs (15,7 s)
    time is over
    '''
    n = int(n)
    if (n * 2) % 2 != 0:
        return 0
    total = 0
    sum_arr = []
    sum_of_nines = n * 9

    def sum_of_digits(number):
        string = str(number)
        sum_ = 0
        for i in range(len(string)):
            sum_ += int(string[i])
        return sum_

    for j in range(sum_of_nines + 1):
        sum_arr.append(0)
    for k in range(10 ** n):
        sum_arr[sum_of_digits(k)] += 1
    for m in range(sum_of_nines + 1):
        total += sum_arr[m] ** 2
    return total


count_lucky()
