from utils.test_util import test


@test()
def func(string=None):
    '''
    Test 0 PASSED Time: 14mcs
    Test 1 PASSED Time: 5mcs
    Test 2 PASSED Time: 1mcs
    Test 3 PASSED Time: 2mcs
    Test 4 FAILED Time: 3mcs
    '''
    if string:
        return len(string)
    return 0


func()
