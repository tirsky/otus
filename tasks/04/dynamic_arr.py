class IArray:
    def __init__(self):
        self.array = []

    def size(self):
        return len(self.array)

    def is_empty(self):
        return self.size() == 0

    def get(self, index: int):
        return

    def add(self, item, index: int):
        pass

    def remove(self, index: int):
        return


class SingleArray(IArray):

    def __init__(self):
        super().__init__()
        self.array = []

    def size(self):
        return len(self.array)

    def add(self, item, index=None):
        self._resize()
        self.array[self.size() - 1] = item

    def _resize(self):
        new_array = [0] * (self.size() + 1)
        for i in range(self.size()):
            new_array[i] = self.array[i]
        self.array = new_array

    def get(self, index: int):
        return self.array[index]


class VectorArray(IArray):

    def __init__(self, vector):
        super().__init__()
        self.array = []
        self.vector = vector
        self._size = 0

    def size(self):
        return self._size

    def add(self, item, index=None):
        if self.size() == len(self.array):
            self._resize()
        self.array[self._size] = item
        self._size += 1

    def _resize(self):
        new_array = [0] * (self.size() + self.vector)
        for i in range(self.size()):
            new_array[i] = self.array[i]
        self.array = new_array

    def get(self, index: int):
        return self.array[index]


class FactorArray(IArray):

    def __init__(self):
        super().__init__()
        self.array = [] * 8
        self.factor = 2
        self._size = 0

    def size(self):
        return self._size

    def add(self, item, index=None):
        if self.size() == len(self.array):
            self._resize()
        self.array[self._size] = item
        self._size += 1

    def _resize(self):
        new_array = [0] * (self.size() * self.factor + 1)
        for i in range(self.size()):
            new_array[i] = self.array[i]
        self.array = new_array

    def get(self, index: int):
        return self.array[index]


class MatrixArray(IArray):

    def __init__(self):
        super().__init__()
        self._size = 0
        self.line = 100
        self.box = [[] * self.line]

    def size(self):
        return self._size

    def add(self, item, index=None):
        if self._size == len(self.box) * self.line:
            vector_array = VectorArray(self.line)
            self.box.append(vector_array)
        if len(self.box) == 1:
            self.box[self._size // self.line].append(item)
        else:
            self.box[self._size // self.line].add(item)
        self._size += 1

    def get(self, index: int):
        if len(self.box) == 1:
            return self.box[index // self.line][index % self.line]
        else:
            return self.box[index // self.line].get(index % self.line)


single_array = SingleArray()
vector_array = VectorArray(10)
factor_array = FactorArray()
matrix_array = MatrixArray()


def test_append():
    import time
    t1 = time.time()
    for i in range(10000):
        single_array.add(i)
    t2 = time.time() - t1
    print(t2, single_array.size())
    t1 = time.time()
    for i in range(10000):
        vector_array.add(i)
    t2 = time.time() - t1
    print(t2, vector_array.size())
    t1 = time.time()
    for i in range(10000):
        factor_array.add(i)
    t2 = time.time() - t1
    print(t2, factor_array.size())
    t1 = time.time()
    for i in range(10000):
        matrix_array.add(i)
    t2 = time.time() - t1
    print(t2, matrix_array.size())


test_append()
