from utils.test_util import test


@test()
def gcd_sub(a, b):
    '''
    Test 0 PASSED Time: 21mcs
    Test 1 PASSED Time: 10mcs
    Test 2 PASSED Time: 6221805mcs
    Test 3 PASSED Time: 10356458mcs
    Test 4 PASSED Time: 137770mcs
    Test 5 PASSED Time: 107635mcs
    time is over
    ...
    '''
    a = int(a)
    b = int(b)
    while a - b != 0:
        if a > b:
            a = a - b
        else:
            b = b - a
    return a


print('GCD sub')

gcd_sub()


@test()
def gcd_rem(a, b):
    '''
    Test 0 PASSED Time: 14mcs
    Test 1 PASSED Time: 2mcs
    Test 2 PASSED Time: 2mcs
    Test 3 PASSED Time: 2mcs
    Test 4 PASSED Time: 3mcs
    Test 5 PASSED Time: 2mcs
    Test 6 PASSED Time: 11mcs
    Test 7 PASSED Time: 24mcs
    Test 8 PASSED Time: 24mcs
    Test 9 PASSED Time: 3mcs
    '''
    a = int(a)
    b = int(b)
    while a != 0 and b != 0:
        if a > b:
            if a % b == 0:
                return b
            else:
                a = a % b
        elif b > a:
            if b % a == 0:
                return a
            else:
                b = b % a


print('GCD rem')
gcd_rem()


@test()
def gcd_bit(a, b):
    '''
    Test 0 PASSED Time: 10mcs
    Test 1 PASSED Time: 3mcs
    Test 2 PASSED Time: 21mcs
    Test 3 PASSED Time: 21mcs
    Test 4 PASSED Time: 28mcs
    Test 5 PASSED Time: 23mcs
    Test 6 PASSED Time: 92mcs
    Test 7 PASSED Time: 169mcs
    Test 8 PASSED Time: 209mcs
    Test 9 PASSED Time: 37mcs
    '''
    a = int(a)
    b = int(b)

    if a == 0:
        return b
    if b == 0:
        return a
    if a == b:
        return a
    if a == 1 or b == 1:
        return 1
    nod = 1
    while a != 0 and b != 0:
        if ~a & 1 and ~b & 1:
            nod <<= 1
            a >>= 1
            b >>= 1
            continue
        if ~a & 1 and b & 1:  # Если a - четное, а b - нечетное, то a делим на 2 (или сдвигаем вправо на 1 бит) и присваем а новое значение
            a >>= 1
            continue
        if a & 1 and ~b & 1:
            b >>= 1
            continue
        if (a > b):
            a, b = b, a
        a, b = (b - a) >> 1, a
    if a == 0:
        return nod * b
    else:
        return nod * a


print('GCD bit')
gcd_bit()
