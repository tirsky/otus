from math import exp, log

from utils.test_util import test


@test()
def pow_log(a, n):
    '''
    Test 0 PASSED Time: 21mcs
    Test 1 PASSED Time: 7mcs
    Test 2 PASSED Time: 6mcs
    Test 3 PASSED Time: 6mcs
    Test 4 PASSED Time: 6mcs
    Test 5 PASSED Time: 6mcs
    Test 6 PASSED Time: 5mcs
    Test 7 PASSED Time: 5mcs
    Test 8 PASSED Time: 5mcs
    Test 9 PASSED Time: 6mcs
    '''
    a = float(a)
    n = float(n)
    return round(exp(n * log(a)), 11)


print('POW LOG')


# pow_log()


@test()
def pow_mul(a, n):
    '''
    Test 0 PASSED Time: 10mcs
    Test 1 FAILED Time: 3mcs (из-за float)
    Test 2 PASSED Time: 401mcs
    Test 3 PASSED Time: 3795mcs
    Test 4 PASSED Time: 36481mcs
    Test 5 PASSED Time: 359833mcs
    Test 6 PASSED Time: 3539832mcs
    Test 7 PASSED Time: 36921625mcs
    каждый раз в 10 раз больше - предполагаю - что дальше будет
    369216250mcs
    3692162500mcs - час ждать придется последний тест)
    '''
    r = 1
    i = 0
    while i < float(n):
        r *= float(a)
        i += 1
    return round(r, 11)


print('POW_MUL')


# pow_mul()


@test()
def pow_bin(a, n):
    '''
    Test 0 FAILED Time: 31mcs
    Test 1 FAILED Time: 3mcs
    Test 2 PASSED Time: 25mcs
    Test 3 PASSED Time: 14mcs
    Test 4 FAILED Time: 19mcs
    Test 5 FAILED Time: 19mcs
    Test 6 FAILED Time: 16mcs
    Test 7 FAILED Time: 32mcs
    Test 8 FAILED Time: 26mcs
    Test 9 FAILED Time: 27mcs
    '''
    try:
        a = int(a)
    except ValueError:
        a = float(a)
    try:
        n = int(n)
    except ValueError:
        n = float(n)
    res = 1
    while n > 1:
        if n % 2 == 0:
            a *= a
            n /= 2
        else:
            res *= a
            n -= 1
    if n > 0:
        res *= a
    return round(res, 11)


print('POW BIN')

# pow_bin()

print('POW BIN-MUL')


@test()
def pow_bin_mul(a, n):
    '''
    Test 0 PASSED Time: 286mcs
    Test 1 FAILED Time: 6mcs
    Test 2 PASSED Time: 269mcs
    Test 3 PASSED Time: 264mcs
    Test 4 FAILED Time: 266mcs
    Test 5 FAILED Time: 255mcs
    Test 6 FAILED Time: 272mcs
    Test 7 FAILED Time: 254mcs
    Test 8 FAILED Time: 227mcs
    Test 9 FAILED Time: 278mcs
    Тесты все показали почти точное соответствие тестовым данным кроме 11-12 знака, дело в округлении, а так тесты проходят
    '''
    b = float(a)
    p = 1
    n = int(n)
    while n:
        b *= b
        n /= 2
        if int(n) % 2 == 1:
            p *= b
    return round(p, 11)


pow_bin_mul()
