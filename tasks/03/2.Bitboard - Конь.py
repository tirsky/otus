from utils.test_util import test


@test()
def knight(x=None):
    '''
    Testing 2.Bitboard - Конь...
    Test 0 PASSED Time: 18mcs
    Test 1 PASSED Time: 7mcs
    Test 2 PASSED Time: 11mcs
    Test 3 PASSED Time: 11mcs
    Test 4 PASSED Time: 9mcs
    Test 5 PASSED Time: 11mcs
    Test 6 PASSED Time: 9mcs
    Test 7 PASSED Time: 21mcs
    Test 8 PASSED Time: 8mcs
    Test 9 PASSED Time: 26mcs
    PASSED 10
    FAILED 0
    100% PASSED
    '''
    x = int(x)
    k = 1 << x

    na = 0xFeFeFeFeFeFeFeFe
    nab = 0xFcFcFcFcFcFcFcFc
    nh = 0x7f7f7f7f7f7f7f7f
    ngh = 0x3f3f3f3f3f3f3f3f

    mask = ngh & (k << 6 | k >> 10) \
           | nh & (k << 15 | k >> 17) \
           | na & (k << 17 | k >> 15) \
           | nab & (k << 10 | k >> 6)

    count = 0
    total = mask
    while mask:
        count += 1
        mask &= mask - 1
    return str(count) + '\n' + str(total)


knight()
