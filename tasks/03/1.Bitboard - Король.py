from utils.test_util import test


@test()
def king(x=None):
    '''
    Test 0 PASSED Time: 26mcs
    Test 1 PASSED Time: 9mcs
    Test 2 PASSED Time: 8mcs
    Test 3 PASSED Time: 8mcs
    Test 4 PASSED Time: 8mcs
    Test 5 PASSED Time: 10mcs
    Test 6 PASSED Time: 11mcs
    Test 7 PASSED Time: 8mcs
    Test 8 PASSED Time: 8mcs
    Test 9 PASSED Time: 8mcs
    PASSED 10
    FAILED 0
    100% PASSED
    '''
    x = int(x)
    k = 1 << x
    kL = k & 0xfefefefefefefefe
    kR = k & 0x7f7f7f7f7f7f7f7f
    kU = k & 0xffffffffffffff
    kD = k & 0xffffffffffffff00
    mask = ((kL & kU) << 7) | (kU << 8) | ((kR & kU) << 9) | \
           (kL >> 1) | (kR << 1) | \
           ((kL & kD) >> 9) | (kD >> 8) | ((kR & kD) >> 7)

    count = 0
    total = mask
    while mask:
        count += 1
        mask &= mask - 1
    return str(count) + '\n' + str(total)


king()
