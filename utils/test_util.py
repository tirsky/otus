import os
import sys
import time
from functools import wraps

from dotenv import load_dotenv
from termcolor import colored

load_dotenv()


def test():
    test_name = sys.argv[0].split('/')[-1].split('.py')[0]  # get test name from task name
    print(f'Testing {test_name}...')

    def _test(f):
        @wraps(f)
        def inner(*args, **kwargs):
            passed = 0
            failed = 0
            for address, dirs, files in os.walk(os.environ['HOME_TEST_DIR']):
                for dir in dirs:
                    if not dir == test_name:
                        continue
                    path = os.path.join(address, dir)
                    for address, dirs, files in os.walk(path):
                        for file in sorted(files):
                            if file.startswith('test') and file.endswith('in'):
                                full_path = os.path.join(address, file)
                                test_number = file.split('.')[1]
                                with open(full_path) as ff:
                                    resource = ff.read()
                                    if '\n' in resource:
                                        resource = resource.split('\n')
                                        resource = [x for x in resource if len(x)]
                                    t1 = time.time_ns() // 1000
                                    if isinstance(resource, str) and resource.isdigit():
                                        result = f(resource)
                                    else:
                                        result = f(*resource)
                                    t2 = time.time_ns() // 1000
                                full_path_out = os.path.join(address, f'test.{test_number}.out')
                                with open(full_path_out) as ff:
                                    test_out = ff.read()
                                if str(result).strip() == str(test_out).strip():
                                    print(colored(f'Test {test_number} PASSED', 'green'),
                                          f'Time: {round(t2 - t1, 3)}mcs')
                                    passed += 1
                                else:
                                    print(colored(f'Test {test_number} FAILED', 'red'),
                                          f'Time: {round(t2 - t1, 3)}mcs')
                                    failed += 1
            print(colored(f'PASSED {passed}', 'green'))
            print(colored(f'FAILED {failed}', 'red'))
            print(f'{int(float(passed / (passed + failed) * 100))}% PASSED')

            print()

        return inner

    return _test
